import React, { Component } from 'react';
import $ from "jquery";


class App extends React.Component {
    constructor(props) {
        super(props);

        let self = this;
        self.state = {
            marker: [],
            markers: [],
            lines: [],
        };
        self.map = {};
        self.LastPolyLine = {};

        window.ymaps.ready(function(){
            self.map = new window.ymaps.Map('map',{
                center: [54.3282400, 48.3865700],
                zoom: 10
            });
        });
    }

    componentDidMount() {
        this.handleResize();
        this.setState();

        window.addEventListener('resize',this.handleResize.bind(this));
    }

    shouldComponentUpdate(nextProps, prevState) {
        var self = this;

        if (nextProps.marker && nextProps.marker.length > 0) {
            let newMarker = self.createMarker(nextProps.marker);
            self.createLine(newMarker);
        } else if (nextProps.deleteMarkerId) {
            self.deleteMarker(nextProps.deleteMarkerId);
            self.addLine();
        } else {
            self.state.markers = nextProps.markers;
            self.getLines();
            self.addLine();
            self.UpdateMarkers();
        }

        return true;
    }


    /**
     * Событие ресайза
     */
    handleResize() {
        let self = this;

        $('#map').css({'width': (window.innerWidth - $('.dot-main').offsetWidth), 'height': window.innerHeight});
    }

    /**
     * Отрисовка маркера
     * @param {{object}} marker
     * @returns {T | *}
     */
    createMarker(marker) {
        let self = this;

        marker = marker.pop();

        marker.coordinates = self.map.getCenter();

        let myPlacemark = new window.ymaps.Placemark(marker.coordinates, {
            balloonContentHeader: marker.title,
        }, {
            draggable: true,
        });

        myPlacemark.events.add('dragend', (e) => {
            let coordinates = myPlacemark.geometry.getCoordinates();

            self.state.markers.map((markerItem) => {
                if (markerItem.id === marker.id) {
                    markerItem.coordinates = coordinates;
                }
            });

            // self.UpdateMarkers();

            self.getLines();

            self.addLine();

            self.props.changeMarkers(self.state.markers);
        });

        marker.Placemark = myPlacemark;
        self.map.geoObjects.add(myPlacemark);

        return marker;
    }

    /**
     * Удаление маркера с карты
     * @param {{integer}} removeId id удаляемого маркера
     * @returns {*|*[]}
     */
    deleteMarker(removeId) {
        let self = this;

        let deleteMarker = self.state.markers.filter((marker) => marker.id === removeId);
        deleteMarker = deleteMarker.pop();
        self.state.markers = self.state.markers.filter((marker) => marker.id !== removeId);
        self.state.lines = self.state.lines.filter((line) => line[0] !== deleteMarker.coordinates[0] && line[1] !== deleteMarker.coordinates[1]);

        self.map.geoObjects.remove(deleteMarker.Placemark);

        return deleteMarker;
    }

    UpdateMarkers() {
        let self = this;

        // self.state.markers.map((marker) => {
        //     // self.map.geoObjects.remove(marker.Placemark);
        //     self.createMarker([marker]);
        // });
    }

    /**
     * Удаление линии и отрисовка новой по новым координатам
     * @param {{object}} marker маркер
     */
    createLine(marker) {
        let self = this;

        self.state.lines.push(marker.coordinates);

        let Polyline = self.addLine();

        self.map.geoObjects.remove(self.LastPolyLine);
        self.LastPolyLine = Polyline;
        self.map.geoObjects.add(Polyline);

        marker.polyline = Polyline;
        self.state.markers.push(marker);
    }

    /**
     * Добавлении линии на карту
     * @returns {Polyline|*}
     */
    addLine() {
        let self = this;

        let Polyline = new window.ymaps.Polyline(
            self.state.lines,
            {},
            {
                strokeColor: "#000000",
                strokeWidth: 4,
                strokeOpacity: 0.3
            }
        );

        self.map.geoObjects.remove(self.LastPolyLine);
        self.LastPolyLine = Polyline;
        self.map.geoObjects.add(Polyline);

        return Polyline;
    }

    getLines() {
        let self = this;

        self.state.lines = [];

        self.state.markers.map((marker) => {
            self.state.lines.push(marker.coordinates);
        });
    }

    render() {
        let self = this;

        return (
            <div className="Map" id="map">

            </div>
        );
    }
}

export default App;