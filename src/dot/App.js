import React, { Component } from 'react';
import {SortableContainer, SortableElement, SortableHandle, arrayMove} from 'react-sortable-hoc';

import $ from "jquery";
import MapApp from '../map/App';


class App extends React.Component {
    constructor(props) {
        super(props);

        let self = this;

        self.state = {
            value: '',
            dots: [],
            showingInfoWindow: false,
            activeMarker: {},
            selectedPlace: {},
            markers: [],
            marker: [],
            Polyline: [],
            deleteMarkerId: null,
        };

        self.handleNewAddress = this.newAddress.bind(this);
    }


    /**
     * Добавление нового точки
     * @param {{object}} event
     */
    newAddress(event) {
        let self = this;
        let value = $(event.target).val();

        if (event.key === 'Enter') {
            self.state.value = value;

            // let coordinate = value.split(' ');

            // if (coordinate.length === 2 && coordinate[0] && coordinate[1]) {
                let oldMarker = this.state.marker;

                oldMarker.length = 0;

                let markerId = new Date().getTime();

                let tmpMarker = {
                    id: markerId,
                    title: value,
                    // coordinates: [
                    //     parseFloat(coordinate[0]),
                    //     parseFloat(coordinate[1]),
                    // ],
                };

                oldMarker.push(tmpMarker);

                let newMarkers = self.state.markers;
                self.state.deleteMarkerId = null;

                newMarkers.push(tmpMarker);
                self.setState({marker: oldMarker, markers: newMarkers, deleteMarkerId: null});

                $(event.target).val('')
            // }
        }
    }

    /**
     * Удаление точки
     * @param {{integer}} id
     * @param {{object}} event
     */
    removeDot(id, event) {
        event.stopPropagation();
        let self = this;
        let newMarkers = self.state.markers.filter((marker) => {
            return marker.id !== id;
        });

        self.state.markers = newMarkers;
        self.state.marker = [];
        self.state.deleteMarkerId = id;
        self.setState({markers: newMarkers, marker: [], deleteMarkerId: id});
    }

    /**
     * Событие на изменение маркера(вызывается из дочернего компонента)
     * @param {{array}} markers
     */
    changeMarkers(markers) {
        let self = this;

        self.state.markers = markers;
        self.setState({markers: markers});
    }

    /**
     * Событие завершения сортировки
     * @param {{integer}} oldIndex
     * @param {{integer}} newIndex
     */
    onSortEndMarkers = ({oldIndex, newIndex}) => {
        let self = this;
        let newMarkers = arrayMove(self.state.markers, oldIndex, newIndex);

        self.state.markers = newMarkers;
        self.setState({markers: newMarkers});
    };

    render() {
        let self = this;

        let DragHandle = SortableHandle(({title})=><span className={title}>{title}</span>);

        let SortableItem = SortableElement(({value, onClickRemove}) =>
            <div>
                <DragHandle title={value.title} />
                <span className="dot-remove" onClick={(e) => onClickRemove(value.id, e)}>x</span>
            </div>
        );

        let SortableList = SortableContainer(({items}) => {
            return (
                <div className="dot-list">
                    {items.map((value, index) => (
                        <SortableItem key={`item-${index}`} index={index} value={value} onClickRemove={self.removeDot.bind(this)} />
                    ))}
                </div>
            );
        });


        return (
            <div className="dashboard clearfix">
                <div id="dot" className="dot-main">
                    <div className="dot">
                        <input type="text" onKeyPress={self.handleNewAddress}/>
                        <div className="dot-list">
                            <SortableList items={self.state.markers} onSortEnd={self.onSortEndMarkers} useDragHandle={true} />
                        </div>
                    </div>
                </div>
                <div className="map-main">
                    <MapApp marker={self.state.marker} changeMarkers={self.changeMarkers.bind(self)} markers={self.state.markers} deleteMarkerId={self.state.deleteMarkerId}/>
                </div>
            </div>
        );
    }
}

export default App