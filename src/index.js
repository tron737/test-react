import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import Map from './map/App';
import Dot from './dot/App';
import $ from "jquery";
import registerServiceWorker from './registerServiceWorker';

// ReactDOM.render(<App />, $('#root')[0]);
ReactDOM.render(<Dot />, $('.dashboard')[0]);
// ReactDOM.render(<Map />, $('#map')[0]);

registerServiceWorker();
